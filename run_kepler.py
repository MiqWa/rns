#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Help: ./RNS -options<meaning> {default value} 

      -r <central density>     {1.28e-3}
      -a <axes ratio rp/re>    {1.0}
      -x max(rp/re) -y min(rp/re) -z delta(rp/re) 
      -l min(rhoc)  -m max(rhoc)  -n delta(rhoc) 

      -t <EOS type: poly/tab>  {poly} 
      -f <tab EOS file>        {no file} 
      -k <poly EOS K>          {100.0}
      -g <poly EOS Gamma>      {2.0}

      -d <{A} diff>            {1.0}
      -u  (set uniform rotation)

      -e<accuracy goal>       {1e-7}

      -i <initial_model_data.hdf> 
      -o <final_model_data.hdf> 
      -s <export_data_file.txt>
"""
import os
import sys

home=str(os.getcwd())

# creating directories
path=home+'/results'
try:
    os.mkdir(path)
except OSError as error:
    print(error)
    
path=path+'/BUmodels'
try:
    os.mkdir(path)
except OSError as error:
    print(error)

# EOS definition
f=open('par_eos.d','r')
n_eos=f.readline().split('	')[0]
f.readline()

if n_eos == '1':
    poly_k=f.readline().split('	')[0]
    poly_gamma=f.readline().split('	')[0]
    eos_stuff = ' -t poly -k '+poly_k+' -g '+poly_gamma+' '
else:
    path_eos=f.readline().split('	')[0]
    eos_stuff = ' -f '+path_eos+' '

f.close()


# READING FROM FILE THE INITIAL DATA
f=open('par_rns.d','r')
f.readline()
name=f.readline().split('	')[0]
n_fix=f.readline().split('	')[0]
rho_c=f.readline().split('	')[0]
rho_min=f.readline().split('	')[0]
rho_max=f.readline().split('	')[0]
rho_delta=f.readline().split('	')[0]
ratio=f.readline().split('	')[0]
ratio_min=f.readline().split('	')[0]
ratio_max=f.readline().split('	')[0]
ratio_delta=f.readline().split('	')[0]
n_rot=f.readline().split('	')[0]
A_hat=f.readline().split('	')[0]
out_h5_ini=f.readline().split('	')[0]
out_h5_final=f.readline().split('	')[0]
f.readline()
n_mass=f.readline().split('	')[0]
fix_mass=f.readline().split('	')[0]
f.readline()
n_kepler=f.readline().split('	')[0]

f.close()

if n_fix == '1':
    fixvar=' -r '+rho_c+' '
    minim=' -y '+ratio_min+' '
    maxim=' -x '+ratio_max+' '
    delta=' -z '+ratio_delta+' '
    fixvar=fixvar+minim+maxim+delta
elif n_fix == '2':
    fixvar = ' -a '+ratio+' '
    minim = ' -l '+rho_min+' '
    maxim = ' -m '+rho_max+' '
    delta = ' -n '+rho_delta+' '
    fixvar = fixvar+minim+maxim+delta
else:
    fixvar1 = ' -r '+rho_c+' '
    fixvar2 = ' -a '+ratio+' '
    fixvar = fixvar1+fixvar2



if n_rot == '1':
    A_hat = ' -d '+A_hat+' '
else:
    A_hat =' -u '


output_text_file = ' -s '+name+'_out.txt '

	
if out_h5_ini != '0':
    h5_file_ini = '-i '+out_h5_ini+'.h5 '
else : 
    h5_file_ini = ' '


if out_h5_final != '0':
    h5_file_final = ' -o '+name+'_TOT.h5 '
else : 
    h5_file_final = ' '    

#1:rhoc 2:ec 3:rp_re 4:A_diff 5:Re 6:M0 7:M 8:W 9:T 10:J 11:beta 12:M_R 13:Omega_K 14:Omega_c 15:Omega_e 16:r_e 17:invPa 18:invPe 19:invtd 

rhomax=0.00366025403784
rhomin=0.00056776436283
drho=9.90195135928e-05
iniratio=0.8

f=open('M_e_kepler.txt','w')
f.write('# central enery density [c=G=M_sun=1] \t Gravitatioal Mass [M_sun]')
f.close()

while(rhomin<rhomax):
    fixvar1 = ' -r '+str(rhomin)+' '
    fixvar2 = ' -a '+str(iniratio)+' '
    fixvar = fixvar1+fixvar2

    dh=0.1

    while(True):
        os.system('./RNS '+fixvar+A_hat+eos_stuff+output_text_file+h5_file_ini+h5_file_final)

        f=open(name+'_out.txt','r')
        i=1
        for xx in f:
            if i==7:
                data=xx.split(' ')
            i+=1
        f.close()

        check=float(data[13])
        fix_kep=float(data[12])
        old=float(data[2])

        if float(format(check,'.4f')) == float(format(fix_kep,'.4f')) :
            break

        if (dh > 0 and check > float(fix_kep)) or (dh < 0 and check < float(fix_kep)) :
            dh=-dh*0.5

        new=old-dh
        fixvar2 = ' -a '+format(new,'.4f')+' '
        fixvar = fixvar1+fixvar2

    f1=open(name+'_out.txt','r')
    i=1
    for xx in f1:
        if i==7:
            data=xx.split(' ')
        i+=1
    f1.close()

    f2=open('M_e_kepler.txt','a')
    f2.write('\n'+str(data[1])+'\t'+str(data[6]))
    f2.close()

    rhomin+=drho
    print('\n\n*******************************')
    print('siamo a rho='+str(rhomin))
    print('*******************************\n\n')


#####################

print('FINITO')









