################### CONFIGURATION FOR RNS ##################################
AU5	Name of the run
3	Fixed variable: 1 Central density, 2 Axes ratio, 3 Both
7.991e-4	Central density [c=g=M_sun=1]
0.15e-3	MIN Central density [c=g=M_sun=1]
1.28e-3	MAX Central density [c=g=M_sun=1]
0.1e-3	Delta central density
0.575	Axes ratio [rp/re]
0.5	MIN Axes ratio [rp/re]
0.9	MAX Axes ratio [rp/re]
0.1	Delta axes ratio
2	Differential rotation: 1 yes, 2 no
1	Rotation parameter A_hat=R0/Req
0	Initial model data .h5 file name (press 0 if not)
0	Final model data .h5 file name (0 if not, 1 if yes)
################### FIX MASS ###############################################
0	Checking for fix baryon mass (0 if not, 1 if yes)
1.506	Fixed baryon mass M0 [M_sun]
#################### KEPLER ###############################################
0	Checking kepler limit (0 if not, 1 if yes)
