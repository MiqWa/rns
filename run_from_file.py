#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 09:44:07 2021

@author: miquelmiravet
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Help: ./RNS -options<meaning> {default value} 

      -r <central density>     {1.28e-3}
      -a <axes ratio rp/re>    {1.0}
      -x max(rp/re) -y min(rp/re) -z delta(rp/re) 
      -l min(rhoc)  -m max(rhoc)  -n delta(rhoc) 

      -t <EOS type: poly/tab>  {poly} 
      -f <tab EOS file>        {no file} 
      -k <poly EOS K>          {100.0}
      -g <poly EOS Gamma>      {2.0}

      -d <{A} diff>            {1.0}
      -u  (set uniform rotation)

      -e<accuracy goal>       {1e-7}

      -i <initial_model_data.hdf> 
      -o <final_model_data.hdf> 
      -s <export_data_file.txt>
"""
import os
import numpy as np

path = str(os.getcwd())
arx = path+'/par_rns.d'
ini_data = np.loadtxt(arx,skiprows = 2)

if ini_data[1] == 0 and ini_data[2] == 0:
    print('Fixed central density, rho_c = ', ini_data[0])
if ini_data[5] == 0 and ini_data[6] == 0:
    print('Fixed axes ratio, rp/re = ', ini_data[4])
if ini_data[8] == 0:
    print('Uniform rotation!')
else :
    print('Differential rotation: A = ', ini_data[8])


if ini_data[1] == 0 and ini_data[2] == 0 and ini_data[5] != 0 and ini_data[6] != 0:
    fixvar = '-r '+str(ini_data[0])+' '
    minim = '-y '+str(ini_data[5])+' '
    maxim = '-x '+str(ini_data[6])+' '
    delta = '-z '+str(ini_data[7])+' '
    fixvar = fixvar+minim+maxim+delta
    
elif ini_data[1] != 0 and ini_data[2] != 0 and ini_data[5] == 0 and ini_data[6] == 0:
    fixvar = '-a '+str(ini_data[4])+' '
    minim = '-l '+str(ini_data[1])+' '
    maxim = '-m '+str(ini_data[2])+' '
    delta = input('Delta central density : ')
    delta = '-n '+str(ini_data[3])+' '
    fixvar = fixvar+minim+maxim+delta
    
elif ini_data[1] == 0 and ini_data[2] == 0 and ini_data[5] == 0 and ini_data[6] == 0:
    fixvar1 = '-r '+str(ini_data[0])+' '
    fixvar2 = '-a '+str(ini_data[4])+' '
    fixvar = fixvar1+fixvar2
    
if ini_data[8] == 0:
    A_hat ='-u '

else: 
    A_hat = '-d '+str(ini_data[8])+' '
    
arx_eos = path+'/par_eos.d'

with open(arx_eos) as f:
    which_eos = f.readlines()

which_eos = np.float(which_eos[1])

if which_eos == 0:
    print(which_eos)
    data_eos = np.loadtxt(arx_eos, skiprows = 3)
    poly_k  = data_eos[0]
    poly_gamma = data_eos[1]
    eos_stuff = '-t poly -k '+str(poly_k)+' -g '+str(poly_gamma)+' '

elif which_eos == 1:
    with open(arx_eos) as f:
        data = f.readlines()
    data_eos = data[2]
    eos_stuff = '-f '+data_eos+' '
print(poly_k)   
#accuracy_goal = '-e '    
#initial_model_data_file = '-i '
#final_model_data_file = '-o '

out_file = input('Output .txt file name : ')

output_text_file = '-s '+out_file+'.txt '

out_h5_final = input('Final model data .h5 file name (press 0 if not): ')
if out_h5_final != '0':
    h5_file_final = '-o '+out_h5_final+'.hdf '
else : 
    h5_file_final = ' '    

os.system('./RNS '+fixvar+A_hat+eos_stuff+output_text_file+h5_file_final)

os.system('mv '+out_h5_final+'* results/.')
os.system('mv '+out_file+'* results/.')
